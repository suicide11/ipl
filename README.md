## How to run the app
``` 
npm install
npm start
```
The App will use port 3000 if it is free


## Framework and Libraries

* React Js
* Antd
* Moment

## Bonous features

- [ ] Created the app using VueJs
- [X] Optimised the loading time using Hash Map
- [X] Mobile Responsive
- [X] Progressive web-app.
- [X] Offline Usable 

## Hosted on

* https://iplatlan.netlify.app/ (Netlify)
* http://13.127.96.73:8080/ (AWS)