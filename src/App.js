import React, { Component } from 'react';
import './App.css';
import 'antd/dist/antd.css';
import TopNav from './component/topNav'
import DataTable from './component/table'
import { Table, Tag } from 'antd';
import data from './data.json'
import Card from './component/card'
import Thumbnail from './component/thumbnail'
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      current: 2017,
      view: "card",
      width: window.screen.height,
      selectedTeam: false
    }
  }
  componentDidMount() {
    window.addEventListener('resize', this.handleScreen);
    var IPLData = {}
    var teams = {}
    data.map(val => {
      if (val.season in IPLData) {
        val.key = val.id
        IPLData[val.season].push(val)
        teams[val.season].add(val.team1)
        teams[val.season].add(val.team2)
      }
      else {
        val.key = val.id
        teams[val.season] = new Set()
        teams[val.season].add(val.team1)
        teams[val.season].add(val.team2)
        IPLData[val.season] = [val]
      }
    })

    this.setState({
      data: IPLData,
      teams: (teams)
    })
    console.log(IPLData, teams)
  }

  handleNav = (data) => {
    this.setState({
      current: data,
      selectedTeam :false
    })
  }
  handleView = (data) => {
    this.setState({
      view: data
    })
  }
  handleScreen = () => {
    if (window.innerWidth < 720)
      this.setState({
        view: "card"
      })
  }
  handleTeamSelect = (data) => {
    this.setState({
      selectedTeam : data
    })
  }
  render() {
    console.log(this.state)
    return (
      <div>
        {this.state.data != null && <TopNav handleView={this.handleView} handleNav={this.handleNav} data={this.state.data} />}
        <div className="body" >
          {this.state.data == null ? (
            <center></center>
          ) : (
              this.state.view == "table" ? (<React.Fragment>
                <div className="season-overall-detail" >
                  <div>
                    Total Matches
                  <br />
                    {this.state.data[this.state.current].length}
                  </div>
                  <div>
                    Total Teams
                  <br />
                    {this.state.teams[this.state.current].size}
                  </div>
                </div>
                <DataTable data={this.state.data[this.state.current]} />
              </React.Fragment>) :
                (
                  <React.Fragment>
                    <div className="season-overall-detail" >
                      <div>
                        Total Matches
                  <br />
                        {this.state.data[this.state.current].length}
                      </div>
                      <div>
                        Total Teams
                  <br />
                        {this.state.teams[this.state.current].size}
                      </div>
                    </div>
                    <div className="card-layout" >
                      {this.state.selectedTeam ? (
                        <React.Fragment>
                          <center>
                          {Array.from(this.state.teams[this.state.current]).map(val =>
                            <Tag onClick={()=>this.handleTeamSelect(val)} color="magenta" > {val} </Tag> 
                          )}
                          </center>
                          {
                            this.state.data[this.state.current].map(val =>
                              <React.Fragment>
                                {(val.team1 == this.state.selectedTeam || val.team2 == this.state.selectedTeam) && <Card data={val} />} 
                              </React.Fragment>
                            )
                          }
                        </React.Fragment>
                      ) : (
                          <React.Fragment>
                            <center>
                              {Array.from(this.state.teams[this.state.current]).map(val =>
                                <Thumbnail handleTeamSelect={this.handleTeamSelect} src={val} > </Thumbnail>
                              )}
                            </center>
                          </React.Fragment>
                        )}

                    </div>
                  </React.Fragment>
                )
            )}
        </div>
      </div>
    )
  }
}

export default App;
