import React, { Component, Fragment } from 'react'
import { Table, Badge, Menu, Dropdown, Space, Button } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import moment from 'moment'
import Detail from './detail'

class DataTable extends Component {
    state = {
        detail: false
    }
    handleDetail = (data) => {
        this.setState({
            detail: data
        })
    }
    render() {
        const colmnsdropdown = [
            {
                title: 'Umpire 1',
                dataIndex: 'umpire1',
            },
            {
                title: 'Umpire 2',
                dataIndex: 'umpire2',
            },
            {
                title: 'Umpire 3',
                dataIndex: 'umpire3',
            },
            {
                title: 'Venue',
                dataIndex: 'venue',
            },
            {
                title: 'Dl Applied',
                dataIndex: 'dl_applied',
            },
            {
                title: 'Result',
                dataIndex: 'result',
            },
            {
                title: 'Player Of Match',
                dataIndex: 'player_of_match',
            },
        ]
        const columns = [
            {
                title: 'Date',
                dataIndex: 'date',
                sorter: {
                    compare: (a, b) => moment(a.data) - moment(b.date),
                    multiple: 1,
                },
            },
            {
                title: 'Team 1',
                dataIndex: 'team1',
            },
            {
                title: 'Team 2',
                dataIndex: 'team2',
            },
            {
                title: 'city',
                dataIndex: 'city',
                sorter: {
                    compare: (a, b) => a.city - b.city,
                    multiple: 1,
                },
            },

            {
                title: 'Winner',
                dataIndex: 'winner',
            },

            {
                title: 'Win By Runs',
                dataIndex: 'win_by_runs',
                sorter: {
                    compare: (a, b) => a.win_by_runs - b.win_by_runs,
                    multiple: 2,
                },
            },

            {
                title: 'Win By Wicket',
                dataIndex: 'win_by_wickets',
                sorter: {
                    compare: (a, b) => a.win_by_wickets - b.win_by_wickets,
                    multiple: 3,
                },
            },
        ];
        return (
            <Fragment>
                <Table
                
                className="components-table-demo-nested" 
                expandable={{
                    expandedRowRender: (record) => <Table dataSource={[record]} pagination={false}  columns={colmnsdropdown} ></Table>
                  }}
                columns={columns} dataSource={this.props.data} />
                {this.state.detail && <Detail data={this.state.detail} />}
            </Fragment>
        )
    }
}

export default DataTable