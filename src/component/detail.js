import React, { Component } from 'react'
import moment from 'moment'
class Detail extends Component {
    render() {
        return (
            <div className="detail" >
                <h2 style={{ float: "left" }}> {this.props.data.team1} </h2>
                <h2 style={{ float: "right" }}> {this.props.data.team2} </h2>
                <div style={{ clear: "both" }} ></div>
                <center>
                    <h3>
                        {moment(this.props.data.date).format("DD-MMMM-YYYY")}
                    </h3>
                </center>
                <hr />
                <div className="data-header" >
                    <div> {this.props.data.city} </div>
                    <div> {this.props.data.umpire1} </div>
                    <div> {this.props.data.umpire2} </div>
                </div>
                <hr />
                <div className="data-body" >
                    <div>
                        <img style={{ height: "100%", width: "100%" }} src="https://cdn.freelogovectors.net/wp-content/uploads/2018/03/Mumbai_Indians_Logo.png" ></img>
                    </div>
                    <div>
                        <p className="matchDetail" >
                            <h1> Win by runs : {this.props.data.win_by_runs}</h1>
                            <h1>{this.props.data.win_by_runs}</h1>
                            <h1>{this.props.data.win_by_runs}</h1>
                            <h1>{this.props.data.win_by_runs}</h1>
                            <h1>{this.props.data.win_by_runs}</h1>
                            <h1>{this.props.data.win_by_runs}</h1>
                            <h1>{this.props.data.win_by_runs}</h1>

                        </p>
                    </div>
                </div>
            </div>
        )
    }
}


export default Detail