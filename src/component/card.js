import React from 'react'
import moment from 'moment'
import { Tag } from 'antd'
function Card({ data }) {
    return (
        <div className="card" >
            <div style={{ display: "flex" }} >
                <div className="team-detail team1">
                    <img style={{ width: "100%" }} src ={"/images/"+data.team1.replaceAll(" ","") +".png"} ></img>
                </div>
                <div className="match-detail" >
                    <h4>Umpires</h4>
                    <br />
                    {data.umpire1}
                    <br />
                    {data.umpire2}
                    <br />
                    {data.umpire3}
                </div>
                <div className="match-detail" >
                    <h4>Winner</h4>
                    <br />
                    <b style={{fontWeight:"900"}} >{data.winner}</b>
                    <br />
                    {data.win_by_wickets==0?(
                        <b> {data.win_by_runs} Runs  </b>
                    ):(
                        <b> {data.win_by_wickets} Wicket </b>
                    )}
                </div>
                <div className="match-detail">
                    <h4>Toss</h4>
                    <br />
                    {data.toss_winner}
                    <br />
                    {data.toss_decision}
                </div>
                <div className="team-detail team2">
                    <img style={{ width: "100%" }} src ={"/images/"+data.team2.replaceAll(" ","") +".png"} ></img>
                </div>
            </div>
            <center style={{width:"100%", display:"block ruby", overflowX:"scroll"}}>
            <Tag color="blue">{moment(data.date).format("DD-MMMM-YYYY")}</Tag>
            <Tag color="blue">  {data.city} </Tag>
            <Tag color="blue">  {data.venue} </Tag>
            <Tag color="geekblue"> Man of the match : {data.player_of_match} </Tag>
            {data.dl_applied ==0 ? (
               
                <Tag color="green">  Normal Result </Tag>
            ):(
            <Tag color="red">  DL applied </Tag>
            )}
            </center>
        </div>
    )
}

export default Card