import React, { component } from 'react'
import { Menu } from 'antd';
import { CalendarOutlined } from '@ant-design/icons';
import { Switch } from 'antd';
const { SubMenu } = Menu;

class TopNav extends React.Component {
  state = {
    current: '2017',
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({ current: e.key });  
    this.props.handleNav(e.key)
  };

  onChange=(checked)=> {
    if(checked) {
      this.props.handleView("card")
    }
    else {
      this.props.handleView("table")
    }
  }

  render() {
    console.log(this.state)
    const { current } = this.state;
    return (
      <Menu className="topNav" theme={"dark"}  selectedKeys={[current]} mode="horizontal">

        <SubMenu key="SubMenu" icon={<CalendarOutlined />} title={this.state.current}>
          {Object.keys(this.props.data).map((key) =>
            <Menu.Item onClick={(e) => this.handleClick(e)} key={key} >
              {key}
            </Menu.Item>
          )}
        </SubMenu>
        <Switch
          style ={{float:"right", margin:"12px"}}
          defaultChecked
          checkedChildren={<> Card </>}
          unCheckedChildren={<> Table </>}
          onChange={this.onChange}
        />
      </Menu>
    );
  }
}

export default TopNav