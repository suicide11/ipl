import React from 'react'

function Thumbnail(props) {
    return(
        <img onClick = {()=>props.handleTeamSelect(props.src)} className="thumbnail" src={"/images/"+props.src.replaceAll(" ","") +".png"}  ></img>
    )
}

export default Thumbnail